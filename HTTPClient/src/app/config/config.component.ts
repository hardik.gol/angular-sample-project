import { Component, OnInit } from '@angular/core';
import { ConfigService , Config} from '../config.service';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css'],
  providers: [ ConfigService ]
})
export class ConfigComponent implements OnInit {

  config: Config;
  configResponse: any;
  config_3: any;
  config_2: any;
  error: any;

  constructor(private configService: ConfigService) { }

  ngOnInit() {
    this.showConfig();
    this.showConfig_1();
    this.showConfig_2();
    this.showConfig_3();
    this.showConfig_4();
  }

  showConfig() {
    this.configService.getConfig()
      .subscribe((data: Config) => {
        console.log('configService.getConfig');
        console.log(data);
        this.config = {
          heroesUrl: data['heroesUrl'],
          textfile:  data['textfile']
      }; }
    );
  }
  showConfig_1() {
    this.configService.getConfigResponse()
    .subscribe((data: any) => {
      console.log('configService.getConfigResponse');
      console.log(data);
      data = data.body;
      this.configResponse = {
        heroesUrl: data['heroesUrl'],
        textfile:  data['textfile']
    }; },
    (error) => {
      this.error = error;
    }
  );
  }

    showConfig_2() {
      this.configService.getConfig_3()
      .subscribe((data: any) => {
          console.log('configService.getConfig_3');
          console.log(data);
          this.config_2 = {
            heroesUrl: data['heroesUrl'],
            textfile:  data['textfile']
        }; },
        (error) => {
          this.error = error;
        }
      );
    }

    showConfig_3() {
      this.configService.getConfig_retry()
      .subscribe((data: any) => {
          console.log('configService.getConfig_retry');
          console.log(data);
          this.config_3 = {
            heroesUrl: data['heroesUrl'],
            textfile:  data['textfile']
        }; },
        (error) => {
          this.error = error;
        }
      );
    }
    showConfig_4() {
      this.configService.getConfig_TextFile()
      .subscribe((data: any) => {
          console.log('configService.getConfig_TextFile');
          console.log(data);
        },
        (error) => {
          this.error = error;
        }
      );
    }
}
