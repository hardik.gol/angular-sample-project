import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

export interface Config {
  heroesUrl: string;
  textfile: string;
}

@Injectable()
export class ConfigService {
  configUrl = 'assets/config.json';
  logError(arg0: any, arg1: any): any {
    console.error('FileName ' + arg0 + ' error ' + arg1);
  }
  log(arg0: any, arg1: any): any {
    console.log('FileName ' + arg0 + 'Data ' + arg1);
  }
  constructor(private httpClient: HttpClient) { }

  getConfig() {
    return this.httpClient.get(this.configUrl);
  }
  getConfigResponse() {
    return this.httpClient.get(this.configUrl, { observe: 'response' });
  }
  getConfig_3() {
    return this.httpClient.get(this.configUrl).pipe(catchError(this.handleError));
  }
  getConfig_retry() {
    return this.httpClient.get<Config>(this.configUrl + 'makeWrongError')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  getConfig_TextFile() {
    return this.httpClient.get(this.configUrl, {responseType: 'text'})
      .pipe(
        tap( // Log the result or error
          data => this.log('filename', data),
          error => this.logError('filename', error)
        )
      );
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
