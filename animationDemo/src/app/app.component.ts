import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { Hero } from './Hero';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('heroState', [
      state('inactive', style({
        backgroundColor: '#eee',
        transform: 'translateX(0)'
      })),
      state('active',   style({
        backgroundColor: '#cfd8dc',
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(100)
      ]),
      transition('active <=> inactive', [
        style({transform: 'translateX(-100%)'}),
        animate('1000ms ease-in', style({transform: 'translateX(0)'}))])
    ])
  ]
})
export class AppComponent {
  title = 'app';
  heroes: Hero[] = [];
  HERO_NAME = ['h1', 'h2'];
  constructor() {
    this.heroes = this.HERO_NAME.map((name) => new Hero(name));
  }
  animationStarted(event) {
    console.log(`animationStarted ` , event);
  }
  animationDone(event) {
    console.log(`animationDone `, event);
  }
}
