import { Injectable } from '@angular/core';

@Injectable()
export class InstancePerComponentService {
  name;
  constructor() { }
  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }
}
