import { Injectable } from '@angular/core';
import { ParentService} from './parent.service';

@Injectable()
export class DerivedService extends ParentService {
  serviceName = 'DerivedService';

  constructor() {
    super();
  }
  getHome() {
    const home = super.getHome();
    home.address = 'Santa clara';
    return home;
  }
}
