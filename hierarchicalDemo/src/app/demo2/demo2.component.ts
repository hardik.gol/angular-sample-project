import { Component } from '@angular/core';
import { ParentService } from './parent.service';
import { DerivedService } from './derived.service';

@Component({
  selector: 'app-demo2',
  template: `
    <p>{{at}}</p>
    <app-subdemo2></app-subdemo2>
  `,
  styles: [],
  providers: [ParentService]
})
export class Demo2Component {
  at;
  constructor(private parentService: ParentService) {
    this.at = this.parentService.serviceName + ' ' + this.parentService.getHome().at;
  }

}

@Component({
  selector: 'app-subdemo2',
  template: `<p>{{at}}</p>
  `,
  styles: [],
  providers: [ DerivedService ]
})
export class Demo2SubComponent {
  at;
  constructor(private derivedService: DerivedService) {
    this.at = this.derivedService.serviceName + ' ' + this.derivedService.getHome().at;
  }
}
