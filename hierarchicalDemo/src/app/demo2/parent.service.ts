import { Injectable } from '@angular/core';
export class Home {
  address: string;
  pin: string;
  constructor(address, pin) {
    this.address = address;
    this.pin = pin;
  }
  get at() {
    return `${this.address} ${this.pin}`;
  }
}

@Injectable()
export class ParentService {
  serviceName = 'ParentService';
  constructor() { }
  getHome() {
    return new Home('street 11', '12345');
  }
  describedHomeService() {
    return `${this.serviceName}`;
  }
}
