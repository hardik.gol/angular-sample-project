import { Component, OnInit, Input } from '@angular/core';
import { InstancePerComponentService } from '../../instance-per-component.service';
@Component({
  selector: 'app-child',
  template: `
    <p>
    {{name}}
    </p>
  `,
  styles: [],
  providers : [ InstancePerComponentService ]
})
export class ChildComponent implements OnInit {

  constructor(private service: InstancePerComponentService) { }

  ngOnInit() {
  }

  @Input(name)
  set name(name) {
    this.service.setName(name);
  }
  get name () {
    return this.service.getName();
  }

}
