import { Component, OnInit } from '@angular/core';
import { SingleInstanceService } from '../single-instance.service';
@Component({
  selector: 'app-demo1',
  template: `
    <app-child *ngFor="let name of names" [name]='name'></app-child>
  `,
  styles: []
})
export class Demo1Component implements OnInit {
  names = [];
  constructor(private singleInstance: SingleInstanceService) {
    this.names = this.singleInstance.getNames();
    console.log(this.names);
  }

  ngOnInit() {
  }

}
