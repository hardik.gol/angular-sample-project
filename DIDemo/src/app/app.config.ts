import { InjectionToken } from '@angular/core';

export interface AppConfig {
    apiEndpoint: string;
    title: string;
}

export const HERO_DI_CONFIG: AppConfig = {
    apiEndpoint: 'api.heroes.com',
    title: 'Dependency Injection'
};

export const TOKEN =
  new InjectionToken('desc', { providedIn: 'root', factory: () => HERO_DI_CONFIG });
