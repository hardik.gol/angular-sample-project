import { Injectable } from '@angular/core';
let instanceCounter = 0;
@Injectable({
  providedIn: 'root'
})
export class HeroService {
  message = '';
  constructor() {
    instanceCounter++;
    console.log(instanceCounter);
  }
  getMessage() {
    return this.message;
  }
  setMessage(msg) {
    this.message = msg;
  }
}
