import { Component, OnInit, Injector } from '@angular/core';
import { HeroService } from '../hero.service';
import { TOKEN } from '../../app.config';

@Component({
  selector: 'app-hero',
  template: `
    <p>
      hero works!
    </p>
  `,
  styles: []
})
export class HeroComponent implements OnInit {

  constructor(private heroService: HeroService, private injector: Injector) {
    console.log('HeroComponent constructor() : getMessage ' + this.heroService.getMessage());
    const config = this.injector.get(TOKEN);
    const heroServiceFromInjector = this.injector.get(HeroService);
    console.log('HeroComponent constructor() : getMessage From Injector HeroService ' + heroServiceFromInjector.getMessage());
    console.log('HeroComponent constructor() : read Token ' + config.title);
   }

  ngOnInit() {
  }

}
