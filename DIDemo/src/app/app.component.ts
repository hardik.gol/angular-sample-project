import { Component, Inject } from '@angular/core';
import { HeroService } from './heroes/hero.service';
import { TOKEN } from './app.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app';
  constructor(private heroService: HeroService, @Inject(TOKEN) config) {
    this.heroService.setMessage('AppComponent constructor called.');
    console.log('AppComponent constructor() :  read Token ' + config.title);
  }
}
