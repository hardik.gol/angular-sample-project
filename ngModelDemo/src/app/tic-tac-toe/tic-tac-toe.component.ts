import { Component } from '@angular/core';
import { TictactoeService } from './tictactoe.service';
@Component({
  selector: 'app-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.css']
})
export class TicTacToeComponent {
  mat = [[2, 3, 4], [5, 6, 7], [8, 9, 22]];
  state = 0;
  msg;
  constructor(private ticTacToe: TictactoeService) {
  }
  reset() {
    this.mat = [[2, 3, 4], [5, 6, 7], [8, 9, 22]];
    this.msg = '';
  }
  toggelStatus() {
    this.state =  this.state === 1 ? 0 : 1;
    this.msg = this.ticTacToe.checkForStatus(this.mat);
  }
}
