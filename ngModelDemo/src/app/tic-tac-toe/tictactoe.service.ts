import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TictactoeService {
  constructor() { }
  checkForStatus(mat): string {
    let msg;
    if (mat[0][0] === mat[0][1] && mat[0][1] === mat[0][2]) {
      msg = 'Winner is ' + mat[0][0];
    } else if (mat[0][0] === mat[1][0] && mat[1][0] === mat[2][0]) {
      msg = 'Winner is ' +  mat[0][0];
    } else if (mat[1][0] === mat[1][1] && mat[1][1] === mat[1][2]) {
      msg = 'Winner is ' + mat[1][0];
    } else if (mat[0][1] === mat[1][1] && mat[1][1] === mat[2][1]) {
      msg = 'Winner is ' + mat[0][1];
    } else if (mat[0][2] === mat[1][2] && mat[1][2] === mat[2][2]) {
      msg = 'Winner is ' + mat[0][2];
    } else if (mat[0][2] === mat[1][2] && mat[1][2] === mat[2][2]) {
      msg = 'Winner is ' + mat[0][2];
    } else if (mat[0][0] === mat[1][1] && mat[1][1] === mat[2][2]) {
      msg = 'Winner is ' + mat[0][0];
    } else if (mat[0][2] === mat[1][1] && mat[1][1] === mat[2][0]) {
      msg = 'Winner is ' + mat[0][2];
    }
    return msg;
  }
}
