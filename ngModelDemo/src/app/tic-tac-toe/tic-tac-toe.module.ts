import { NgModule, OnChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicTacToeComponent } from './tic-tac-toe.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [TicTacToeComponent],
  exports: [TicTacToeComponent]
})
export class TicTacToeModule {}
