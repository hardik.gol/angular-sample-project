import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mySort',
  pure:false
})
export class MySortPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value instanceof Array && value.length > 0) {
      let orderBy = args ? args :'';
      console.log(value[0] , ' isObject ',value[0] instanceof Object);
      value[0] instanceof Object ? this.sortObject(value, orderBy) : this.sortArray(value, orderBy);
    }
    return value;
  }

  sortArray(array:Array<Number>,orderBy?):Array<Number> {
    console.log(`sortArray`);
    let _self = this;
    array.sort((a,b):number => {
      return _self.mySort(a,b);}
    );
    return array;
  }

  sortObject(array:Array<Number>,orderBy:string):Array<Number> {
    console.log(`sortObject`, orderBy);
    let _self = this;
    array.sort((a:any,b:any):number => {
      return _self.mySort(a[orderBy],b[orderBy])}
    );
    return array;
  }

  mySort(a:any,b:any):number {
    console.log(`mySort`,a,b);
    if(a < b)
      return -1;
    else if(a > b)
      return 1;
    else
      return 0;
  }

}
