import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MySortPipe } from './my-sort.pipe';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'app';
  unsortedData = [1,5,2,7,3,4];
  unsortedObjectData = [{"name":"a"},{"name":"f"},{"name":"c"},{"name":"s"}]
  objectData = [{"name":"a"},{"name":"f"},{"name":"c"},{"name":"s"}];
  data = [1,5,2,7,3,4];

  constructor() {
  }

  ngOnInit() {
    console.log(`ngOnInit`);
  }

  addRandomValue() {
    console.log(`addRandomValue :`,Math.random());
    let randomNumber = Math.floor(Math.random() * (999999 - 100000)) + 10;
    this.unsortedData = this.unsortedData.concat([randomNumber]);
    this.data = this.data.concat([randomNumber]);
  }
}
