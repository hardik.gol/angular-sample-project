import { Component, OnInit, Input, Output, OnChanges, SimpleChange, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnChanges {
  @Input() message: any;
  @Input('messageId') messageId: string;
  @Output() messageReceivied = new EventEmitter<any>();
  messageReadStatus: string;
  _messageType: string;
  changeLog: string[] = [];
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    // tslint:disable-next-line:prefer-const
    let log: string[] = [];
    // tslint:disable-next-line:forin
    // tslint:disable-next-line:prefer-const
    // tslint:disable-next-line:forin
    for (const propName in changes) {
      const changedProp = changes[propName];
      const to = JSON.stringify(changedProp.currentValue);
      if (changedProp.isFirstChange()) {
        log.push(`Initial value of ${propName} set to ${to}`);
      } else {
        const from = JSON.stringify(changedProp.previousValue);
        log.push(`${propName} changed from ${from} to ${to}`);
      }
    }
    this.changeLog.push(log.join(', '));
  }

  @Input()
  set messageType(messageType: any) {
    this._messageType = messageType;
  }

  notifyToParent() {
    this.messageReadStatus = JSON.stringify({'messageId': this.messageId, 'isReceived': true});
    this.messageReceivied.emit({'messageId': this.messageId, 'isReceived': true});
  }
}
