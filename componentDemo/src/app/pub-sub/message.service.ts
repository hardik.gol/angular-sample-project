import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable(
// {
  // providedIn: 'root'
// }
)
export class MessageService {
  pubMessage = new Subject();
  pubMessageAck = new Subject();
  pubMessage$ = this.pubMessage.asObservable();
  pubMessageAck$ = this.pubMessageAck.asObservable();
  constructor() { }
  publishMessage(msg) {
    this.pubMessage.next(msg);
  }
  ackMessage(message: any) {
    console.log('MService ', message);
    console.log('MService ', message.id, message.subscrname);
    this.pubMessageAck.next({'id': message.id, 'subscriberName': message.subscrname});
  }
}
