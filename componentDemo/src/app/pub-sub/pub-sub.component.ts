import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';
@Component({
  selector: 'app-pub-sub',
  templateUrl: './pub-sub.component.html',
  styleUrls: ['./pub-sub.component.css'],
  providers : [ MessageService]
})
export class PubSubComponent implements OnInit {
  message: string;
  messageId = 0;
  subscriberName: string;
  subscribersList: string[] = [];
  successFullyPublishMessage: string[] = [];
  constructor(private messageService: MessageService) {
    this.messageService.pubMessageAck$.subscribe((messageAck: any) => {
      console.log(messageAck);
      console.log(`Message Id ${messageAck.id} is successfully recevied by subscriber ${messageAck.subscriberName}`);
      this.successFullyPublishMessage.push(
        `Message Id ${messageAck.id} is successfully recevied by subscriber ${messageAck.subscriberName}`);
    });
  }

  ngOnInit() {
  }
  onPublish() {
    this.messageService.publishMessage({'message': this.message, 'id': this.messageId++});
  }
  addSubscriber() {
    this.subscribersList.push(this.subscriberName);
  }
}
