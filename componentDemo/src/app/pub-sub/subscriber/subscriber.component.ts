import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MessageService } from '../message.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css'],
  // providers: [ MessageService ]
})
export class SubscriberComponent implements OnInit, OnDestroy {
  @Input() subscriberName: string;
  messages: string[] = [];
  subscription: Subscription;
  constructor(private messageService: MessageService) {
    this.subscription = this.messageService.pubMessage$.subscribe((message: any) => {
      console.log('subComponent ' + message);
      // tslint:disable-next-line:prefer-const
      console.log(message);
      message.subscrname = this.subscriberName;
      this.messages.push(message);
      this.messageService.ackMessage(message);
    });
  }

  ngOnInit() {
    console.log(`${this.subscriberName} OnInit`);
  }
  ngOnDestroy() {
    console.log(`${this.subscriberName} OnDestroy`);
    this.subscription.unsubscribe();
  }
}
