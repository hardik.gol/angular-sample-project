import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  message = 'pass message to child component';
  messageId = 'dummy';
  messageType = 'text';
  isReceived = false;
  notifyMessage = '';
  constructor() { }

  ngOnInit() {
  }

  messageAck(message: any) {
    const msg = message;
    this.isReceived = msg.isReceived;
    this.notifyMessage = `MessageId ${msg.messageId} is ${this.isReceived ? '' : 'not'} received`;
  }
}
