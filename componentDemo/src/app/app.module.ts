import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { PubSubComponent } from './pub-sub/pub-sub.component';
import { SubscriberComponent } from './pub-sub/subscriber/subscriber.component';
import { CountdownViewChildParentComponent } from './countdown-view-child-parent/countdown-view-child-parent.component';
import { CountdownTimerComponent } from './countdown-view-child-parent/countdown-timer/countdown-timer.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    PubSubComponent,
    SubscriberComponent,
    CountdownViewChildParentComponent,
    CountdownTimerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
